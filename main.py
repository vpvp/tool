from tkinter import *
from function import *
from tkinter import ttk
import getpass

username = getpass.getuser()
print("******************************Validation Tool******************************")
print("Welcome", username)

root = Tk()
root.title("Validation Tool")
root.geometry("600x300")
menubar = Menu(root)
menu = Menu(menubar, relief=RAISED)


def details():

    acc_name_label = Label(root, text="Account name")
    acc_name_label.place(x=5, y=10)

    acc_name_data = Label(root, text=ClassAccount.acc_name)
    acc_name_data.place(x=150, y=10)

    no_loc_label = Label(root, text="No of Location")
    no_loc_label.place(x=5, y=40)

    no_loc_data = Label(root, text=ClassAccount.no_location)
    no_loc_data.place(x=150, y=40)

    curr_label = Label(root, text="Currency")
    curr_label.place(x=5, y=70)

    curr_data = Label(root, text=ClassAccount.curr)
    curr_data.place(x=150, y=70)


menu.add_command(label="Import Account", command=ClassAccount.import_file)
menu.add_command(label="Run Analysis", command=details)
menu.add_command(label="Quit", command=root.quit)

for i in range(22):
    menu.add_separator()
    i = i+1

menu.add_command(label=username)
root.config(menu=menu)
frame = Frame(root)
frame.pack()
bottom_frame = Frame(root)
bottom_frame.pack(side=TOP)
root.protocol(root.quit, AccountClass.on_closing)
root.mainloop()
