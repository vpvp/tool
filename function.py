from tkinter.filedialog import askopenfilename
from tkinter import Tk
from tkinter import messagebox
import pandas as pd
from tkinter import ttk
from tkinter import *
import numpy as np
import time


class AccountClass:

    def __init__(self, filename=None, acc_name=None, no_location=None, curr=None, loc_tab=None, detail_tab=None,
                 progress=None, progress_root=None, locname=None):

        self.filename = filename
        self.acc_name = acc_name
        self.no_location = no_location
        self.curr = curr
        self.loc_tab = loc_tab
        self.detail_tab = detail_tab
        self.progress = progress
        self.progress_root = progress_root
        self.locname = locname

    def progress_bar(self):
        self.progress_root = Tk()
        self.progress_root.title("Importing...")
        self.progress = ttk.Progressbar(self.progress_root, orient=HORIZONTAL, length=400)
        self.progress.pack()
        self.progress_root.overrideredirect(True)
        self.progress.config(mode='indeterminate')

    def import_file(self):
        # ClassAccount.progress_bar()

        self.filename = askopenfilename(title='Select an Account File')

        # self.progress.start()
        # self.progress_root.update_idletasks()

        print(self.filename)
        if len(self.filename) == 0:
            print("No File has been Selected")
        print("File Imported")
        self.loc_tab = pd.read_excel(self.filename, 'LOC')
        self.detail_tab = pd.read_excel(self.filename, 'Details')

        self.acc_name = self.detail_tab.iloc[2][2]
        print(self.detail_tab.iloc[2][2])

        self.no_location = len(self.loc_tab.UNIQUEID)                                # No Of Location
        self.curr = self.loc_tab['EQCV1VCUR'].unique()                               # Currency
        # self.progress.stop()
        # self.progress_root.destroy()
        messagebox.showinfo("Account Imported", "Completed")
        ClassAccount.validation()
        return self.acc_name

    def on_closing(self):
        root = Tk()
        root.quit()
        root.destroy()
        print("Closed")

    def coming_soon(self):
        messagebox.showinfo("Under Development", "Coming Soon")
        # ClassAccount.validation()

    def validation(self):

        null_cell = self.loc_tab.columns[self.loc_tab.isnull().any()]
        print(type(null_cell))
        null_cell_count = self.loc_tab[null_cell].isnull().sum()
        null_list = ['UNIQUEID', 'LOCNAME', 'CNTRYSCHEME', 'CNTRYCODE', 'BLDGSCHEME', 'OCCSCHEME', 'NUMBLDGS',
                     'NUMSTORIES', 'YEARBUILT', 'YEARUPGRAD', 'EQSLINS', 'FRSPRINKLERSYS', 'SPNKLRTYPE', 'FLOORAREA',
                     'AREAUNIT', 'TIV', 'TIV RANK']

        print("-----------Blank Cells------------")

        for n_l in null_list:
            if n_l in null_cell_count:
                print(n_l, null_cell_count[n_l])

        count = 0
        if self.loc_tab.CNTRYSCHEME != 'ISO2A':
            count = count+1
        print("Country Scheme Should be ISO2A", count, "Error Found")


ClassAccount = AccountClass()

